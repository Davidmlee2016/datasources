$updateSession = new-object -com "Microsoft.Update.Session";
$updates=$updateSession.CreateupdateSearcher().Search(("IsInstalled=0 and Type='Software'")).Updates;
$countCritical = 0;
foreach ($update in $updates){if ($update.AutoSelectOnWebSites) {$countCritical++ }};
$countCritical | out-file -filepath C:\LMCriticalUpdates\crit.txt 
